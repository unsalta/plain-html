randomName = document.getElementById("randomName");
randomTitle = document.getElementById("randomTitle");

var dictionary=[];
var typewriterFooter = new Typewriter(randomName, {
    cursor: ''
});

var typewriterAbout = new Typewriter(randomTitle, {
    cursor: ''
});

function setup(){
    noCanvas();
    noLoop();
    dictionary = loadJSON('resources/dictionary.json', type);
}

function randomWord(){
    var keys = Object.keys(dictionary);
    var randIndex = Math.floor(Math.random() * keys.length);
    var randKey = keys[randIndex];
    var l = dictionary[randKey].length;
    var r = Math.floor(Math.random()*l);
    var name = dictionary[randKey][r];
    if (randIndex ==1){
        return("a "+name);
        }
    if (randIndex==2) {
        return("'"+name);
        } 
    else {
            return (" "+ name);
        }
    }

function type(){    
    randomType();
    setInterval(doRandom, 6000);
}

function doRandom(){
    randomType();
    randomTit();
}

function randomType(){
    typewriterFooter.typeString(' *') //with an extra blank space to insert the female '
        .pauseFor(2500)
        .deleteAll()
        .typeString(randomWord())
        .pauseFor(2000)
        .deleteAll()
        .start();
}

function randomTit(){
    typewriterAbout.typeString(' *') //with an extra blank space to insert the female '
        .pauseFor(2500)
        .deleteAll()
        .typeString(randomWord())
        .pauseFor(2000)
        .deleteAll()
        .start();
}
