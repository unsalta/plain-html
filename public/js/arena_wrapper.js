var boxes = [];
var zindex = [];
var myArr = [];
var authors = {};
var icons = [];
var sprite = 10;
var total = 12;
var tinta;
var getChannel = "un-salta";

function fillArray() {
  for (i = 0; i < sprite; i++) {
    icons[i] = i;
  }
}

function loadDoc() {
  fillArray();
  document.getElementById("arena-info").innerHTML +=
    '<a style="color: var(--light-color)" href="https://www.are.na/un-salta/' +
    getChannel +
    '"> powered by Are.na</a>';
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      myArr = JSON.parse(this.responseText);
      tinta = Math.floor(Math.random() * 100);
      for (i = 0; i < myArr.contents.length; i++) {
        boxes.push(createDiv(i));
      }
      boxes.splice(boxes.length - 1, 1);
      console.log(boxes);
    }
  };
  xhttp.open(
    "GET",
    "https://api.are.na/v2/channels/" +
      getChannel +
      "?v=" +
      Math.random() +
      `&per=${total}`,
    true
  ); // il random è pe non cachare la richiesta che altrimenti non mostra i blocchi nuovi

  xhttp.send();
}

function createDiv(n) {
  var userID = myArr.contents[n].user.id;
  var icon;
  if (authors.hasOwnProperty(userID)) {
    icon = authors[userID];
  } else {
    var id = Math.floor(random() * icons.length);
    icons.splice(id);
    icon = "author" + id;
    authors[userID] = icon;
  }
  boxes[n] = document.createElement("div");
  boxes[n].setAttribute("id", myArr.contents[n].id);
  boxes[n].setAttribute("class", "box");
  boxes[n].setAttribute("onmousedown", "stack(this)");
  boxes[n].innerHTML = marked(myArr.contents[n].content);
  boxes[n].style.position = "absolute";
  boxes[n].style.left = Math.random() * 50 + "%";
  boxes[n].style.top = Math.random() * 50 + "%";
  boxes[n].style.zIndex = myArr.contents.length - n;
  zindex[n] = myArr.contents.length - n;
  document.getElementById("main").appendChild(boxes[n]);

  if (myArr.contents[n].embed) {
    boxes[n].innerHTML = myArr.contents[n].embed.html;
    boxes[n].children[0].setAttribute("width", "100%");
    boxes[n].children[0].setAttribute("height", "100%");
    boxes[n].setAttribute("class", "box cutout");
  } else if (myArr.contents[n].attachment) {
    boxes[n].innerHTML = marked(
      "# <a href=" +
        myArr.contents[n].attachment.url +
        " target='_blank'>" +
        myArr.contents[n].title +
        "</a>→"
    );
  } else if (myArr.contents[n].image) {
    var img = document.createElement("img");
    img.setAttribute("src", myArr.contents[n].image.original.url);
    boxes[n].appendChild(img);
    boxes[n].setAttribute("class", "box cutout");
  } else {
    //custom

    var sign = document.createElement("svg");
    sign.setAttribute("class", "icon");
    var useid = document.createElement("use");
    useid.setAttribute("xlink:href", "assets/icon/authors.svg#" + icon);
    sign.appendChild(useid);
    boxes[n].appendChild(sign);
  }
}

function sortZ(index) {
  var i = boxes.indexOf(index);
  boxes.splice(i, 1);
  boxes.splice(0, 0, index);

  // zIndex for the stack

  for (var j = 0; j < boxes.length; j++) {
    var zin = boxes.indexOf(boxes[j]);
    boxes[j].style.zIndex = zindex[zin];
  }
}

function stack(index) {
  var currentScroll = index.scrollTop;

  sortZ(index);

  // move the box

  event.preventDefault();
  let shiftX = event.clientX - index.getBoundingClientRect().left;
  let shiftY = event.clientY - index.getBoundingClientRect().top;

  index.style.position = "absolute";
  document.body.append(index);

  moveAt(event.pageX, event.pageY);

  // moves the box at (pageX, pageY) coordinates
  // taking initial shifts into account

  function moveAt(pageX, pageY) {
    index.style.left = pageX - shiftX + "px";
    index.style.top = pageY - shiftY + "px";
  }

  function onMouseMove(event) {
    moveAt(event.pageX, event.pageY);
  }

  // move the box on mousemove
  document.addEventListener("mousemove", onMouseMove);

  // drop the box, remove unneeded handlers
  index.onmouseup = function() {
    document.removeEventListener("mousemove", onMouseMove);
    index.onmouseup = null;
  };

  index.ondragstart = function() {
    return false;
  };
  index.scrollTop = currentScroll;
}
