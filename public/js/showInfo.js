var about = document.getElementById("about");
var aboutPanel = document.getElementById("aboutPanel");
var contact = document.getElementById("contact");
var contactPanel = document.getElementById("contactPanel");

aboutVisible = false;
contactVisible = false;

function displayAbout() {
  if(aboutVisible){
    aboutPanel.style.visibility = "hidden";
    aboutVisible = !aboutVisible;
    var i = boxes.indexOf(aboutPanel);
    boxes.splice(i,1);
  } else {
  aboutPanel.style.left = Math.random() * 50 + "%";
  aboutPanel.style.top = Math.random() * 50 + "%";
  aboutPanel.style.visibility = "visible";
  aboutVisible = !aboutVisible;
  boxes.push(aboutPanel);
  sortZ(aboutPanel);
  }
}

function displayContact() {
  if(contactVisible){
    contactPanel.style.visibility = "hidden";
    contactVisible = !contactVisible;
    var i = boxes.indexOf(contactPanel);
    boxes.splice(i,1);
  } else {
  contactPanel.style.left = Math.random() * 50 + "%";
  contactPanel.style.top = Math.random() * 50 + "%";
  contactPanel.style.visibility = "visible";
  contactVisible = !contactVisible;
  boxes.push(contactPanel);
  sortZ(contactPanel);
  }
}


about.addEventListener("click", displayAbout);
contact.addEventListener("click", displayContact);
